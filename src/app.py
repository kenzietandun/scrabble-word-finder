#!/usr/bin/env python3

from flask import (Flask, render_template, request)
from shutil import copyfile
import json
import os
import subprocess
from scrabble_finder import ScrabbleFinder

HOME = os.getenv("HOME")
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
finder = ScrabbleFinder()

app = Flask(__name__)
app.secret_key = "4472920f6e0e3e45"

@app.route("/")
def main():
    return render_template("index.html")

@app.route("/find", methods=["POST"])
def find():
    word = request.form["search"]
    result = finder.find(word.lower())
    return render_template("index.html", result=result)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
