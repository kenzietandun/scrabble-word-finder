#!/usr/bin/env python3

from typing import Dict, List, Set
import string
import os
import sys
import search_tree
import pickle


class ScrabbleFinder:
    def __init__(self):

        ALPHABET_PICKLE_FILE = "alphabet_tree.pickle"
        WORD_SET_FILE = "word_set.pickle"

        self.DB_DIR = "db"
        self.word_set: Dict[str, Set[str]] = {}

        if not os.path.isfile(ALPHABET_PICKLE_FILE):
            self.alphabet_tree = search_tree.generate_all_alphabet_search_tree()
            with open(ALPHABET_PICKLE_FILE, "wb") as f:
                pickle.dump(self.alphabet_tree, f, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open(ALPHABET_PICKLE_FILE, "rb") as f:
                self.alphabet_tree = pickle.load(f)

        if not os.path.isfile(WORD_SET_FILE):
            self.populate_word_set()
            with open(WORD_SET_FILE, "wb") as f:
                pickle.dump(self.word_set, f, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            with open(WORD_SET_FILE, "rb") as f:
                self.word_set = pickle.load(f)

        value_letters = {
            1: ["a", "e", "i", "o", "u", "l", "n", "s", "t", "r"],
            2: ["d", "g"],
            3: ["b", "c", "m", "p"],
            4: ["f", "h", "v", "w", "y"],
            5: ["k"],
            8: ["j", "x"],
            10: ["q", "z"],
            0: ["_"],
        }

        self.letter_values: Dict[str, int] = {}
        for score, letters in value_letters.items():
            for letter in letters:
                self.letter_values[letter] = score

        del value_letters

    def populate_word_set(self):
        alphabets = list(string.ascii_lowercase)
        filename = "words-that-start-with-{letter}.txt"
        for letter in alphabets:
            db_filename = filename.format(letter=letter)
            lines = set()
            with open(os.path.join(self.DB_DIR, db_filename), "r") as f:
                for line in f:
                    lines.add(line.rstrip())
            self.word_set[letter] = lines

    def dfs_backtrack(self, candidate, inp, out):
        """
        Thanks COSC262
        """

        """ Backtrack helper function definitions """

        def should_prune(candidate, out, alphabet_tree):
            if not candidate:
                return False

            if candidate in out:
                return True

            if search_tree.search_word(candidate, alphabet_tree):
                return False

            return True

        def add_to_output(candidate, out):
            out.append(candidate)

        def is_solution(candidate, inp):
            if not candidate:
                return False

            starting_letter = candidate[0]
            return candidate in self.word_set[starting_letter]

        def children(candidate, inp: str):
            inp_list = list(inp)
            for letter in candidate:
                if letter not in inp_list:
                    inp_list.remove("_")
                else:
                    inp_list.remove(letter)

            candidate_children = []
            for letter in inp_list:
                if letter == "_":
                    for alphabet in list(string.ascii_lowercase):
                        candidate_children.append(candidate + alphabet)
                else:
                    candidate_children.append(candidate + letter)

            return candidate_children

        if should_prune(candidate, out, self.alphabet_tree):
            return

        if is_solution(candidate, inp):
            add_to_output(candidate, out)

        for child_candidate in children(candidate, inp):
            self.dfs_backtrack(child_candidate, inp, out)

    def calculate_word_score(self, word, inp):
        score = 0
        inp_list = list(inp)
        for letter in word:
            if letter in inp_list:
                score += self.letter_values[letter]
                inp_list.remove(letter)
            else:
                score += self.letter_values["_"]  # 0
                inp_list.remove("_")
        return score

    def find(self, query):
        result: List[str] = []
        self.dfs_backtrack("", query, result)
        result = sorted(
            result, key=lambda x: self.calculate_word_score(x, query), reverse=True
        )

        result_with_score = [(r, self.calculate_word_score(r, query)) for r in result]

        return result_with_score

if __name__ == "__main__":
    f = ScrabbleFinder()
    print(f.find(sys.argv[1]))
