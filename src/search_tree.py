#!/usr/bin/env python3

import os
import string


class Node:
    def __init__(self, letter):
        self.letter = letter
        self.children = dict()

    def __getitem__(self, letter):
        return self.children[letter]

    def __setitem__(self, letter, node):
        self.children[letter] = node

    def __repr__(self):
        return f"Node {self.letter}"

    def __str__(self):
        return f"Node {self.letter} Children: {str(self.children)}\n"

    def has_a_children(self, letter):
        return letter in self.children.keys()

def search_word(word, search_tree):
    is_word = True

    starting_letter = word[0]
    curr_node = search_tree[starting_letter]

    for i, letter in enumerate(word):
        if i == 0:
            continue # skip first letter

        if curr_node.has_a_children(letter):
            curr_node = curr_node[letter]
        else:
            is_word = False
            break

    return is_word

def generate_search_tree(letter):
    filename = "words-that-start-with-{letter}.txt"
    lines = []
    with open(os.path.join("db", filename.format(letter=letter)), "r") as f:
        for line in f:
            lines.append(line.rstrip())

    root_node = Node(letter)

    for line in lines:
        curr_node = root_node
        for i, letter in enumerate(line):
            if i == 0:
                continue  # skip the first letter
            if letter not in curr_node.children.keys():
                curr_node[letter] = Node(letter)
            curr_node = curr_node[letter]

    return root_node

def generate_all_alphabet_search_tree():
    search_tree = dict()
    alphabets = list(string.ascii_lowercase)
    for letter in alphabets:
        search_tree[letter] = generate_search_tree(letter)

    return search_tree
